class ChangePricesToPriceLogs < ActiveRecord::Migration[7.0]
  def change
    rename_table :prices, :price_logs
  end
end
