class CreateMunicipalityPackagePrice < ActiveRecord::Migration[7.0]
  def change
    create_table :municipality_package_prices do |t|
      t.integer :price_cents
      t.references :package, null: false, foreign_key: true
      t.references :municipality, null: false, foreign_key: true

      t.timestamps
      t.index [:package_id, :municipality_id], unique: true, name: 'package_municipality_index'
    end
  end
end
