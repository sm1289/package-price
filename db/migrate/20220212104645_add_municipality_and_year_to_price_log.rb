class AddMunicipalityAndYearToPriceLog < ActiveRecord::Migration[7.0]
  def change
    add_column :price_logs, :year, :integer, null: false
    add_reference :price_logs, :municipality, foreign_key: true

    add_index :price_logs, [:package, :year, :municipality]
  end
end
