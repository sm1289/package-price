# frozen_string_literal: true

require "spec_helper"

RSpec.describe UpdatePackagePrice do
  let(:stockholm_municipality) { Municipality.create!(name: "Stockholm") }
  let(:goteborg_municipality) { Municipality.create!(name: "Göteborg") }
  let(:na_municipality)  { Municipality.create!(name: "NotAvailable") }
  before do
    stockholm_municipality
    goteborg_municipality
    na_municipality
  end

  it "updates the current price of the provided package" do
    package = Package.create!(name: "Dunderhonung")

    UpdatePackagePrice.call(package, 200_00)
    expect(package.price_for).to eq(200_00)
  end

  it "stores the old price of the provided package in its price history" do
    package = Package.create!(name: "Dunderhonung", price_cents: 100_00)

    UpdatePackagePrice.call(package, 200_00)
    expect(package.price_logs).to be_one
    price = package.price_logs.first
    expect(price.price_cents).to eq(200_00)
  end

  it "supports adding a price for a specific municipality" do
    package = Package.create!(name: "Dunderhonung")

    UpdatePackagePrice.call(package, 200_00, municipality: "Göteborg")

    expect(package.price_for("Göteborg")).to eq(200_00)
  end

  it "raise an error if municipality is not created" do
    package = Package.create!(name: "Basic")

    expect{
      UpdatePackagePrice.call(package, 200_00, municipality: "Falun")
    }.to raise_error(ActiveRecord::RecordNotFound)
  end

end
