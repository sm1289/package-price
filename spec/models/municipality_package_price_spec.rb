# frozen_string_literal: true

require "spec_helper"

RSpec.describe MunicipalityPackagePrice do

  let(:municipality) { Municipality.create!(name: "Stockholm") }
  let(:basic) { Package.create!(name: 'Basic') }
  before do
    MunicipalityPackagePrice.create!(municipality: municipality,
                                     package: basic,
                                     price_cents: 100)
  end

  it "validates the uniqueness" do
    municipality_package = MunicipalityPackagePrice.new(municipality: municipality,
                                                        package: basic,
                                                        price_cents: 200)
    expect(municipality_package.validate).to eq(false)
  end

  it "validates the presence of price_cents" do
    municipality_package = MunicipalityPackagePrice.new(price_cents: nil)
    expect(municipality_package.validate).to eq(false)
    expect(municipality_package.errors[:price_cents]).to be_present
  end
end
