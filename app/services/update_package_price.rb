# frozen_string_literal: true

class UpdatePackagePrice
  def self.call(package, new_price_cents, **options)
    Package.transaction do
      municipality_name = options[:municipality] || Municipality::DEFAULT
      municipality = find_municipality(municipality_name)

      municipality_package = MunicipalityPackagePrice.find_or_initialize_by(package: package, municipality: municipality)
      municipality_package.update!(price_cents: new_price_cents)

      # Add a pricing history record
      PriceLog.create!(package: package,
                       price_cents: package.price_for(municipality_name),
                       year: Time.now.year,
                       municipality: municipality)

      # Update the current price
      # TODO: remove price from Package, keeping it here for backward compatibility!
      Package.update!(price_cents: new_price_cents)
    end
  end

  private
    def self.find_municipality(name)
      Municipality.find_by!(name: name)
    end
end
