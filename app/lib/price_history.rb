# frozen_string_literal: true

class PriceHistory
  def self.call(args)
    new(args).call
  end

  attr_reader :package, :year, :municipality

  def initialize(args)
    @package = args[:package]
    @year = args[:year]
    @municipality = args[:municipality]
  end

  def call
    validate_input
    build_history
  end

  def validate_input
    return if package.present? && year.present?

    raise "Package and Year must be present"
  end

  def price_logs
    query_criteria = { package_id: package.id, year: year.to_i }
    query_criteria.merge!({"municipalities.name" =>  municipality}) if municipality

    PriceLog.joins(:municipality)
            .select('price_logs.price_cents, municipalities.name as municipality_name')
            .where(query_criteria).group_by(&:municipality_name)
  end

  def build_history
    result = {}
    price_logs.each do |name, logs|
      result[name] = logs.map(&:price_cents)
    end

    result[municipality] || result
  end
end
