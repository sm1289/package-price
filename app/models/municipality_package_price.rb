# frozen_string_literal: true

class MunicipalityPackagePrice < ApplicationRecord
  belongs_to :package, optional: false
  belongs_to :municipality, optional: false

  validates_uniqueness_of :package, scope: :municipality
  validates :price_cents, presence: true

  def self.price_for(municipality_name)
    # TODO: improve by using join
    find_by!(municipality: Municipality.find_by!(name: municipality_name)).price_cents
  end
end
