# frozen_string_literal: true

class Municipality < ApplicationRecord
  DEFAULT = 'NotAvailable' # NotAvailable to support backward compatibility for short term

  validates :name, presence: true, uniqueness: true
end