# frozen_string_literal: true

class Package < ApplicationRecord
  has_many :price_logs, dependent: :destroy
  has_many :municipality_package_prices, dependent: :destroy

  validates :name, presence: true, uniqueness: true
  validates :price_cents, presence: true # TODO: remove this after discussion

  def price_for(municipality_name = Municipality::DEFAULT)
    municipality_package_prices.price_for(municipality_name)
  end

end
